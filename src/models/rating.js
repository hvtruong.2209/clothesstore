const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");

const Rating = mongoose.model(
  "Rating",
  new Schema(
    {
      order: { type: Schema.Types.ObjectId, ref: "DetailOrder" },
      product: { type: Schema.Types.ObjectId, ref: "Product" },
      point: { type: Number },
    },
    { timestamps: true }
  )
);

function validate(rating) {
  const schema = Joi.object({
    order: Joi.string().required(),
    product: Joi.string().required(),
    point: Joi.number().required(),
  });
  return schema.validate(rating);
}
module.exports.Rating = Rating;
module.exports.validate = validate;
