const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");

const brand = new Schema({
  name: { type: String },
  // image
});

function validate(brand) {
  const schema = Joi.object({
    name: Joi.string(),
  });
  return schema.validate(brand);
}
module.exports.Brand = mongoose.models.Brand || mongoose.model("Brand", brand);
module.exports.validate = validate;
