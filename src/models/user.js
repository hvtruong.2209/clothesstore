const { Schema } = require("mongoose");
function UserSchema(add) {
  var schema = new Schema(
    {
      email: {
        type: String,
        match: [
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          `Please fill valid email address`,
        ],
        required: true,
        unique: true,
      },
      password: { type: String, required: true },
      name: {
        type: String,
        maxlength: 255,
      },
      gender: { type: Boolean, default: 1 },
      birthday: { type: Date },
      phone: { type: String },
      image: { type: String },
      isDeleted: { type: Boolean, default: 0 },
    },
    { timestamps: true }
  );
  if (add) {
    schema.add(add);
  }
  return schema;
}
module.exports = UserSchema;
