const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");

const supplier = mongoose.model(
  "Supplier",
  new Schema({
    name: { type: String },
  })
);

function validate(supplier) {
  const schema = Joi.object({
    name: Joi.string(),
  });
  return schema.validate(supplier);
}
module.exports.Supplier = supplier;
module.exports.validate = validate;
