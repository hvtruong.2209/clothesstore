const { default: mongoose } = require("mongoose");
const UserSchema = require("./user");
const Joi = require("joi");
// const CustomerType = mongoose.model("CustomerType", CustomerTypeSchema());
const Customer = mongoose.model(
  "Customer",
  UserSchema({
    customerType: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CustomerType",
    },
    place: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
  })
);
// const Customer = mongoose.model("Customer", UserSchema());

function validateCustomer(customer) {
  const schema = Joi.object({
    name: Joi.string().max(255),
    email: Joi.string(),
    password: Joi.string(),
    gender: Joi.boolean(),
    birthday: Joi.date(),
    phone: Joi.string()
      .length(10)
      .pattern(/^[0-9]+$/),
    image: Joi.string(),
    customerType: Joi.string(),
  });
  return schema.validate(customer);
}
module.exports.Customer = Customer;
module.exports.validate = validateCustomer;
