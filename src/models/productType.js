const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");
const lodash = require("lodash");
let schema = new Schema({
  name: { type: String },
  // image
});

schema.virtual("countProduct", {
  ref: "Product",
  localField: "_id",
  foreignField: "productType",
  count: true,
});

schema.set("toJSON", { virtuals: true });
schema.set("toObj", { virtuals: true });

schema.methods.toJSON = function () {
  const PICK_FIELDS = ["_id", "name", "countProduct"];
  const detail = this;
  return lodash.pick(detail, PICK_FIELDS);
  //
};
schema.methods.toObjects = function () {
  const PICK_FIELDS = ["_id", "name", "countProduct"];
  const detail = this;
  return lodash.pick(detail, PICK_FIELDS);
  //
};

const type = mongoose.model("ProductType", schema);

function validate(type) {
  const schema = Joi.object({
    name: Joi.string(),
  });
  return schema.validate(type);
}
module.exports.ProductType = type;
module.exports.validate = validate;
