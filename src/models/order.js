const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");
const e = require("express");
const schema = new Schema(
  {
    customer: { type: Schema.Types.ObjectId, ref: "Customer" },
    note: { type: String },
    discount: { type: Schema.Types.ObjectId, ref: "Discount", default: null },
    orderDate: { type: Date, default: Date.now() },
    status: { type: Boolean, default: false },
    phone: { type: String },
    address: { type: Schema.Types.ObjectId, ref: "Place" },
  },
  { timestamps: true }
);

schema.virtual("detailOrders", {
  ref: "DetailOrder",
  localField: "_id",
  foreignField: "order",
  // count: true, return count for field
});

schema.virtual("subTotal").get(function () {
  let subTotal = 0;
  if (this.detailOrders)
    this.detailOrders.forEach(function (el) {
      subTotal += el.price * el.amount;
    });
  return subTotal;
});
//
schema.set("toJSON", { virtuals: true });
// schema.set("toObj", { virtuals: true });

const order = mongoose.model("Order", schema);

function validate(order) {
  const schema = Joi.object({
    customer: Joi.string(),
    note: Joi.string(),
    orderDate: Joi.date(),
    phone: Joi.string()
      .length(10)
      .pattern(/^[0-9]+$/),
    address: Joi.string(),
  });
  return schema.validate(order);
}
module.exports.Order = order;
module.exports.validate = validate;
