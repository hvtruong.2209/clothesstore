const Joi = require("Joi");
const { Schema, default: mongoose } = require("mongoose");

const Place = mongoose.model(
  "Place",
  new Schema({
    no: { type: Number },
    street: { type: String },
    district: { type: String },
    city: { type: String },
  })
);

function validate(place) {
  const schema = Joi.object({
    no: {
      type: Joi.number(),
    },
    street: {
      type: Joi.string(),
    },
    district: {
      type: Joi.string(),
    },
    city: {
      type: Joi.string(),
    },
  });
  return schema.validate(place);
}
module.exports.Place = Place;
module.exports.validate = validate;
