const Joi = require("joi");
const { default: mongoose, Schema } = require("mongoose");

const CustomerType = mongoose.model(
  "CustomerType",
  new Schema({
    name: {
      type: String,
    },
  })
);

function validateCustomerType(customerType) {
  const schema = new Joi.object({
    name: Joi.string().required(),
  });
  return schema.validate(customerType);
}
module.exports.CustomerType = CustomerType;
module.exports.validate = validateCustomerType;
