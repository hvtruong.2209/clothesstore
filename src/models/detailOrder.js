const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");
const lodash = require("lodash");

detailSchema = new Schema(
  {
    order: { type: Schema.Types.ObjectId, ref: "Order" },
    product: { type: Schema.Types.ObjectId, ref: "Product" },
    amount: { type: Number },
  },
  { timestamps: true }
);
//
detailSchema.virtual("price").get(function () {
  return this.product.price;
});
//
detailSchema.methods.toJSON = function () {
  const PICK_FIELDS = ["_id", "order", "product", "amount", "price"];
  const detail = this;
  return lodash.pick(detail, PICK_FIELDS);
  //
};
detailSchema.methods.toObjects = function () {
  const PICK_FIELDS = ["_id", "order", "product", "amount", "price"];
  const detail = this;
  return lodash.pick(detail, PICK_FIELDS);
  //
};
//
const detailOrder = mongoose.model("DetailOrder", detailSchema);
//
function validate(detailOrder) {
  const schema = Joi.object({
    order: Joi.string().required(),
    product: Joi.string().required(),
    amount: Joi.string().required(),
    // price: Joi.date(),
  });
  return schema.validate(detailOrder);
}
module.exports.DetailOrder = detailOrder;
module.exports.validate = validate;
