const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");

const discount = mongoose.model(
  "Discount",
  new Schema(
    {
      name: { type: String },
      percent: { type: Number },
      startDate: { type: Date },
      endDate: { type: Date },
      description: { type: String },
    },
    { timestamps: true }
  )
);

function validate(discount) {
  const schema = Joi.object({
    name: Joi.string(),
    description: Joi.string(),
    percent: Joi.number(),
    startDate: Joi.date(),
    endDate: Joi.date(),
  });
  return schema.validate(discount);
}
module.exports.Discount = discount;
module.exports.validate = validate;
