const { default: mongoose, Schema } = require("mongoose");
const Joi = require("Joi");

const product = mongoose.model(
  "Product",
  new Schema(
    {
      name: { type: String },
      description: { type: String },
      price: { type: Number },
      productType: { type: Schema.Types.ObjectId, ref: "ProductType", default: null },
      // image
      brand: { type: Schema.Types.ObjectId, ref: "Brand", default: null },
      supplier: { type: Schema.Types.ObjectId, ref: "Supplier" },
    },
    { timestamps: true }
  )
);

function validate(product) {
  const schema = Joi.object({
    name: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    productType: Joi.string(),
    brand: Joi.string(),
    supplier: Joi.string(),
  });
  return schema.validate(product);
}
module.exports.Product = product;
module.exports.validate = validate;
