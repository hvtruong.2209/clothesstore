const { DetailOrder } = require("../models/detailOrder");
const BaseService = require("./baseService");

class DetailOrderService extends BaseService {
  get model() {
    return DetailOrder;
  }
  get populate() {
    return "product";
  }
}

module.exports = new DetailOrderService();
