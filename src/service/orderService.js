const { Order } = require("../models/order");
const BaseService = require("./baseService");

class OrderService extends BaseService {
  get model() {
    return Order;
  }
  get populate() {
    return "discount customer address detailOrders";
  }

  async getAll(obj) {
    const page = obj.page;
    const pageSize = obj.pageSize;
    var models = await this.model.find().populate({
      path: "discount customer address detailOrders",
      populate: { path: "product" },
    });
    const totalPage = Math.round(models.length / pageSize);
    if (page && pageSize) {
      const models = await this.model
        .find()
        .skip((page - 1) * pageSize)
        .limit(pageSize)
        .populate(this.populate);
      return {
        result: models,
        totalPage: totalPage,
      };
    }
    return { result: models };
  }
}

module.exports = new OrderService();
