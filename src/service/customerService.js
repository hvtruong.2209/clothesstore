const { Customer } = require("../models/customer");
const BaseService = require("./baseService");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const tokenConfig = require("../const/tokenConfig");

class CustomerService extends BaseService {
  get model() {
    return Customer;
  }
  get populate() {
    return "customerType place";
  }

  async login(email, password) {
    try {
      const customer = await this.model.findOne({
        email: email,
      });
      const isValid = await bcrypt.compare(password, customer.password);
      if (isValid) {
        var token = jwt.sign({ ...customer._doc }, tokenConfig.secret, {
          expiresIn: tokenConfig.tokenLife,
        });
        var refreshToken = jwt.sign({ ...customer._doc }, tokenConfig.refreshTokenSecret, {
          expiresIn: tokenConfig.refreshTokenLife,
        });
        // create freshToken

        return { token, refreshToken };
      }
      return undefined;
    } catch (e) {
      return undefined;
    }
  }
}

module.exports = new CustomerService();
