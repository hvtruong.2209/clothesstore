const { CustomerType } = require("../models/customerType");
const BaseService = require("./baseService");

class CustomerTypeService extends BaseService {
  get model() {
    return CustomerType;
  }
}

module.exports = new CustomerTypeService();
