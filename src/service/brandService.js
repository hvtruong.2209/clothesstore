const { Brand } = require("../models/brand");
const BaseService = require("./baseService");

class BrandService extends BaseService {
  get model() {
    return Brand;
  }
}

module.exports = new BrandService();
