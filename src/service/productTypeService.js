const { ProductType } = require("../models/productType");
const BaseService = require("./baseService");

class ProductTypeService extends BaseService {
  get model() {
    return ProductType;
  }
  get populate() {
    return "countProduct";
  }
}

module.exports = new ProductTypeService();
