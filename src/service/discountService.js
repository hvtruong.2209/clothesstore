const { Discount } = require("../models/discount");
const BaseService = require("./baseService");

class DiscountService extends BaseService {
  get model() {
    return Discount;
  }
}

module.exports = new DiscountService();
