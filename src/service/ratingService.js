const { Rating } = require("../models/rating");
const BaseService = require("./baseService");

class RatingService extends BaseService {
  get model() {
    return Rating;
  }
}

module.exports = new RatingService();
