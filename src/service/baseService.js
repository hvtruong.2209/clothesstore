module.exports = class BaseService {
  constructor() {
    if (!this.model) {
      throw new Error("Model class not provide entity");
    }
  }
  async getAll(obj) {
    const page = obj.page;
    const pageSize = obj.pageSize;
    var models = await this.model.find().populate(this.populate);
    const totalPage = Math.floor(models.length / pageSize) + 1;
    if (page && pageSize) {
      const models = await this.model
        .find()
        .skip((page - 1) * pageSize)
        .limit(pageSize)
        .populate(this.populate);
      return {
        result: models,
        totalPage: totalPage,
      };
    }
    return { result: models };
  }
  async create(obj) {
    // let model = await this.model.create(obj);
    let model = new this.model(obj);
    model = await model.save();
    return model;
  }
  async update(id, obj) {
    let result = await this.model.findOneAndUpdate({ _id: id }, { $set: obj }, { returnDocument: "after" });
    return result;
  }
  async delete(id) {
    const deleted = await this.model.findOneAndDelete({ _id: id }, { returnDocument: "after" });
    return deleted;
  }
  async deleteMany(ids) {
    await this.model.deleteMany({ _id: { $in: ids } });
  }
  async findById(id) {
    const model = await this.model.findById(id).populate(this.populate);
    return model;
  }
};
