const { Product } = require("../models/product");
const BaseService = require("./baseService");

class ProductService extends BaseService {
  get model() {
    return Product;
  }
  get populate() {
    return "productType supplier brand";
  }
}

module.exports = new ProductService();
