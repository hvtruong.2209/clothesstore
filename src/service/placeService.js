const { Place } = require("../models/place");
const BaseService = require("./baseService");

class PlaceService extends BaseService {
  get model() {
    return Place;
  }
}

module.exports = new PlaceService();
