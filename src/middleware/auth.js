const tokenConfig = require("../const/tokenConfig");
const jwt = require("jsonwebtoken");

async function verifyJwtToken(token, secretKey) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return reject(err);
      }
      resolve(decoded);
    });
  });
}

module.exports = async function (req, res, next) {
  try {
    const token = req.header("Authorization").split(" ")[1];
    if (!token)
      return res.status(403).send({
        message: "No token provided!",
      });
    const decoded = await verifyJwtToken(token, tokenConfig.secret);
    req.user = decoded;
    next();
  } catch (err) {
    return res.status(401).json({
      message: "Unauthorized access!",
    });
  }
};
