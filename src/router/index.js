const routerCustomer = require("./customer");
const routerCustomerTypes = require("./customerType");
const routerPlace = require("./place");
const routerProduct = require("./product");
const productType = require("./productType");
const routerBrand = require("./brand");
const routerRating = require("./rating");
const routerDiscount = require("./discount");
const routerDetailOrder = require("./detailOrder");
const routerOrder = require("./order");

function routerApp(app) {
  app.use("/api/v1/customers", routerCustomer);
  app.use("/api/v1/customerTypes", routerCustomerTypes);
  app.use("/api/v1/places", routerPlace);
  app.use("/api/v1/products", routerProduct);
  app.use("/api/v1/productTypes", productType);
  app.use("/api/v1/brands", routerBrand);
  app.use("/api/v1/ratings", routerRating);
  app.use("/api/v1/discounts", routerDiscount);
  app.use("/api/v1/detailOrders", routerDetailOrder);
  app.use("/api/v1/orders", routerOrder);
}

module.exports = routerApp;
