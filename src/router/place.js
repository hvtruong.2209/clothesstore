const express = require("express");
const router = express.Router();
const Controller = require("../controller/placeController");
const auth = require("../middleware/auth");

router.get("/", auth, async (req, res) => {
  Controller.getAll(req, res);
});

router.post("/", auth, async (req, res) => {
  Controller.create(req, res);
});

router.patch("/:id", auth, async (req, res) => {
  Controller.update(req, res);
});

router.delete("/:id", auth, async (req, res) => {
  Controller.delete(req, res);
});
module.exports = router;
