const express = require("express");
const router = express.Router();
const Controller = require("../controller/productTypeController");
const auth = require("../middleware/auth");

router.get("/", auth, async (req, res) => {
  Controller.getAll(req, res);
});

router.post("/", auth, async (req, res) => {
  Controller.create(req, res);
});

module.exports = router;
