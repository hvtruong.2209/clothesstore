const express = require("express");
const router = express.Router();
const Controller = require("../controller/customerController");
const auth = require("../middleware/auth");
const fileSizeLimitErrorHandler = require("../middleware/fileSizeLimitErrorHandler");
const uploadImage = require("../middleware/uploadImage");
var upload = uploadImage.single("image");

router.get("/", auth, async (req, res) => {
  Controller.getAll(req, res);
});

router.post("/:id/upload-image", [auth, upload, fileSizeLimitErrorHandler], async (req, res) => {
  Controller.uploadImage(req, res);
});

router.get("/:fileName", async (req, res) => {
  Controller.getImage(req, res);
});

router.post("/register", async (req, res) => {
  Controller.register(req, res);
});

router.post("/delete-many", auth, async (req, res) => {
  Controller.deleteMany(req, res);
});

router.post("/login", async (req, res) => {
  Controller.login(req, res);
});

router.patch("/:id", auth, async (req, res) => {
  Controller.update(req, res);
});

router.delete("/:id", auth, async (req, res) => {
  Controller.delete(req, res);
});
module.exports = router;
