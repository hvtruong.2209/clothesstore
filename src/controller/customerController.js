const CustomerService = require("../service/customerService");
const { Customer, validate } = require("../models/customer");
const { validate: validatePlace } = require("../models/place");
const bcrypt = require("bcrypt");

const BaseController = require("./baseController");
const { Place } = require("../models/place");

const path = require("path");
class CustomerController extends BaseController {
  get model() {
    return Customer;
  }
  get validate() {
    return validate;
  }
  get service() {
    return CustomerService;
  }

  async login(req, res) {
    const { error } = this.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const check = await this.service.login(req.body.email, req.body.password);
    if (check) return res.status(200).send(check);
    return res.status(400).send("Login failed!");
  }

  async hashPassword(pass) {
    const salt = await bcrypt.genSalt(10);
    const hashed = await bcrypt.hash(pass, salt);
    return hashed;
  }

  async register(req, res) {
    if (req.body.place) {
      var place = {
        no: req.body.place.no,
        street: req.body.place.street,
        district: req.body.place.district,
        city: req.body.place.city,
      };
      delete req.body.place;
      const { err } = validatePlace(place);
      if (err) return res.status(400).send(err.details[0].message);
    }

    const { error } = this.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    req.body.password = await this.hashPassword(req.body.password);

    try {
      if (place) {
        let placeShema = await new Place(place);
        placeShema = await placeShema.save();
        var model = new this.model({
          ...req.body,
          place: placeShema._id,
        });
      } else {
        var model = new this.model({
          ...req.body,
        });
      }
      model = await model.save();
      return res.send(model);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
  async uploadImage(req, res) {
    try {
      if (!req.file) {
        res.status(401).json({ error: "Please provide an image" });
      }
      let imagePath = req.file ? "/src/static/images/customers/" + req.file.filename : undefined;
      const result = await this.service.update(req.params.id, { image: imagePath });
      res.status(200).send(result);
    } catch (err) {
      res.status(400).send(err);
    }
  }
  async getImage(req, res) {
    var options = {
      root: path.join(__dirname, "../../src/static/images/customers"),
      dotfiles: "deny",
      headers: {
        "x-timestamp": Date.now(),
        "x-sent": true,
      },
    };
    var fileName = req.params.fileName;
    res.sendFile(fileName, options, function (err) {
      if (err) {
        res.status(variable.BadRequest).send("Get image false!");
      }
    });
  }
}

module.exports = new CustomerController();
