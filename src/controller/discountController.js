const DiscountService = require("../service/DiscountService");
const { Discount, validate } = require("../models/discount");

const BaseController = require("./baseController");
class DiscountController extends BaseController {
  get model() {
    return Discount;
  }
  get validate() {
    return validate;
  }
  get service() {
    return DiscountService;
  }
}

module.exports = new DiscountController();
