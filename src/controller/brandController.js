const BrandService = require("../service/BrandService");
const { Brand, validate } = require("../models/Brand");

const BaseController = require("./baseController");
class BrandController extends BaseController {
  get model() {
    return Brand;
  }
  get validate() {
    return validate;
  }
  get service() {
    return BrandService;
  }
}

module.exports = new BrandController();
