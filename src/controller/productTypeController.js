const ProductTypeService = require("../service/productTypeService");
const { ProductType, validate } = require("../models/productType");

const BaseController = require("./baseController");
class ProductTypeController extends BaseController {
  get model() {
    return ProductType;
  }
  get validate() {
    return validate;
  }
  get service() {
    return ProductTypeService;
  }
}

module.exports = new ProductTypeController();
