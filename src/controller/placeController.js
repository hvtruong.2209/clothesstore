const PlaceService = require("../service/PlaceService");
const { Place, validate } = require("../models/place");

const BaseController = require("./baseController");
class PlaceController extends BaseController {
  get model() {
    return Place;
  }
  get validate() {
    return validate;
  }
  get service() {
    return PlaceService;
  }
}

module.exports = new PlaceController();
