const CustomerTypeService = require("../service/customerTypeService");
const { CustomerType, validate } = require("../models/customerType");

const BaseController = require("./baseController");
class CustomerTypeController extends BaseController {
  get model() {
    return CustomerType;
  }
  get validate() {
    return validate;
  }
  get service() {
    return CustomerTypeService;
  }
}

module.exports = new CustomerTypeController();
