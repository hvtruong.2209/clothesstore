const OrderService = require("../service/orderService");
const { Order, validate } = require("../models/order");

const BaseController = require("./baseController");
class OrderController extends BaseController {
  get model() {
    return Order;
  }
  get validate() {
    return validate;
  }
  get service() {
    return OrderService;
  }
}

module.exports = new OrderController();
