const DetailOrderService = require("../service/detailOrderService");
const { DetailOrder, validate } = require("../models/detailOrder");

const BaseController = require("./baseController");
class DetailOrderController extends BaseController {
  get model() {
    return DetailOrder;
  }
  get validate() {
    return validate;
  }
  get service() {
    return DetailOrderService;
  }
}

module.exports = new DetailOrderController();
