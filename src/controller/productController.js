const ProductService = require("../service/ProductService");
const { Product, validate } = require("../models/product");

const BaseController = require("./baseController");
class ProductController extends BaseController {
  get model() {
    return Product;
  }
  get validate() {
    return validate;
  }
  get service() {
    return ProductService;
  }
}

module.exports = new ProductController();
