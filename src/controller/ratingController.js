const RatingService = require("../service/RatingService");
const { Rating, validate } = require("../models/rating");

const BaseController = require("./baseController");
class RatingController extends BaseController {
  get model() {
    return Rating;
  }
  get validate() {
    return validate;
  }
  get service() {
    return RatingService;
  }
}

module.exports = new RatingController();
