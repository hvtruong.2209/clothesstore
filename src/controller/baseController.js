module.exports = class BaseController {
  constructor() {
    if (!this.model) {
      throw new Error("Model class not provide entity");
    }
    if (!this.validate) {
      throw new Error("Validate function provide entity");
    }
    if (!this.service) {
      throw new Error("Service class not provide entity");
    }
  }

  async getAll(req, res) {
    const obj = {
      page: req.query.page || null,
      pageSize: req.query.pageSize || null,
    };
    const models = await this.service.getAll(obj);
    res.send(models);
  }

  async getDetail(req, res) {
    const id = req.params.id;
    const models = await this.service.findById(id);
    res.send(models);
  }

  async create(req, res) {
    const { error } = this.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    try {
      let model = await this.service.create(req.body);
      return res.send(model);
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  async update(req, res) {
    const { error } = this.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const id = req.params.id;
    try {
      let result = await this.service.update(id, req.body);
      return res.status(200).send(result);
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  async delete(req, res) {
    try {
      const id = req.params.id;
      const deleted = await this.service.delete(id);
      return res.status(200).send(deleted);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  async deleteMany(req, res) {
    try {
      const ids = req.body.ids;
      await this.service.deleteMany(ids);
      return res.send("Delete successfully");
    } catch (err) {
      res.status(400).send(err);
    }
  }
};
