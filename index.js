const express = require("express");
const app = express();
const mongoose = require("mongoose");
var path = require("path");
// Phân tích cú pháp trung gian trước khi đến quá trình xử lý
const bodyParser = require("body-parser");
const port = process.env.PORT || 3000;
const router = require("./src/router/index");
mongoose
  .connect("mongodb://localhost/ClothesStore")
  .then(() => console.log("Connected to DB"))
  .catch((err) => console.log(err));

// get rep.body
app.use(bodyParser.json());
// Loại trừ stirng và array trong req.body trả về
app.use(bodyParser.urlencoded({ extended: true }));

var dir = path.join(__dirname, "static");
console.log(dir);
app.use(express.static(dir));

router(app);
app.listen(port, () => {
  console.log("Express");
});
